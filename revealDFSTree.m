function dfForest = revealDFSTree(adj)
% depth-first search (graph traversal algorithm) on SCG (based on DFS) /by Moore/
%
% Description: A search algorithm of a graph which explores all nodes
%   children to the current node before moving on.
%
% Remark: 1) it is just a wrapper of DFS
%
% Complexity: Ordo(V+E)
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% Depth-first search (DFS) is an algorithm for traversing or searching a
% tree, tree structure, or graph. One starts at the root (selecting some 
% node as the root in the graph case) and explores as far as possible along 
% each branch before backtracking.
%
% Formally, DFS is an uninformed search that progresses by expanding the
% first child node of the search tree that appears and thus going deeper 
% and deeper until a goal node is found, or until it hits a node that has 
% no children. Then the search backtracks, returning to the most recent node 
% it hasn't finished exploring. In a non-recursive implementation, all 
% freshly expanded nodes are added to a stack for exploration.
%
% Applications:
% - Finding connected components.
% - Topological sorting.
% - Finding 2-(edge or vertex)-connected components.
% - Finding strongly connected components.
% - Solving puzzles with only one solution, such as mazes.
% 
% Exploration sequence:
%            1
%          / | \
%         2  5  6
%        / \   /|\
%       3   4 7 8 9 
% ========================================================================
%
% Syntax:
%   bfTree = revealDFSTree(adj)
%   Input params:
%       adj        - graph in adjacency form
%   Return values:
%       dfForest   - depth-first forest adjacency graph
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23.3, Depth-first search
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: DFS

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --
dfForest = DFS(adj);

% end;