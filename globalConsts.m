function globalConsts()
% globalConsts initializes global constants
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% --- Error Messages ---

% general
global ERRMSG_WRONGINPUTVALUE;
global ERRMSG_WRONGINPUTNUM;

% graph
global ERRMSG_NOTDAG;
global ERRMSG_DIRECTED;
global ERRMSG_NOTDIRECTED;
global ERRMSG_NOTCONNECTED;

% heap
global ERRMSG_HEAPUNDERFLOW;

% queue
global ERRMSG_QUEUEUNDERFLOW;

% matrix
global ERRMSG_NOTVALID;
global ERRMSG_NOTQUADRATIC;

% ============== MSG TEXTS ==============
% general
ERRMSG_WRONGINPUTVALUE = 'Wrong input parameter value!';
ERRMSG_WRONGINPUTNUM = 'Wrong input parameter number!';

% graph
ERRMSG_NOTDAG = 'Wrong input parameter: Not a DAG graph!';
ERRMSG_DIRECTED = 'Wrong input parameter: Not a non-directed graph!';
ERRMSG_NOTDIRECTED = 'Wrong input parameter: Not a directed graph!';
ERRMSG_NOTCONNECTED = 'Wrong input parameter: Not a connected graph!';

% heap
ERRMSG_HEAPUNDERFLOW = 'Heap underflow!';

% queue
ERRMSG_QUEUEUNDERFLOW = 'Prority queue underflow!';

% matrix
ERRMSG_NOTVALID = 'Wrong input parameter: Not a valid matrix!';
ERRMSG_NOTQUADRATIC = 'Wrong input parameter: Not a quadratic matrix!';
