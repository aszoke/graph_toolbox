function mstadj = findMSTPrim(ewadj)
% find the minimum spanning tree (MFT) of graphs on NON-DIRECTED SCG /by Prim/
%
% Remark: 1) it works on NON-DIRECTED G(V,E) single connected graph
%         2) it is a greedy algorithm
%         3) it operates like Dijkstra's algorithm for finding the shortest
%            paths in a graph
%         4) it operates on a priority queue based on a key field (key relates 
%            smallest edge weight): priority queue is used for selecting
%            the smallest priority element
%         5) Kruskal <-> Prim:
%            select min from the union of edge sets <-> 
%            select min priority element (edge) from a priority queue
%            NOTE: selecting from priority queue is faster than from a set
%
% Complexity: Ordo(ElogV)
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% Given a connected, undirected graph, a spanning tree of that graph is a
% subgraph which is a tree and connects all the vertices together. A single 
% graph can have many different spanning trees. We can also assign a weight 
% to each edge, which is a number representing how unfavorable it is, and 
% use this to assign a weight to a spanning tree by computing the sum of the 
% weights of the edges in that spanning tree. A minimum spanning tree (MST) 
% or minimum weight spanning tree is then a spanning tree with weight less 
% than or equal to the weight of every other spanning tree.
% ========================================================================
%
% Syntax:
%   mstadj = findMSTPrim(ewadj)
%   Input params:
%       ewadj   - edge weight adjacency form
%   Return values:
%      mstadj   - minimum spanning tree in adjacency form
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 24.2, Prim's algorithm
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: isDirected, findMSTKruskal

% Copyright 2006-2007

% -- global consts --
global ERRMSG_DIRECTED;
global ERRMSG_NOTCONNECTED;

% create adjacency matrix from weighted adjacency matrix 
% (for input checking)
adj = ewadj2adj(ewadj);

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

if isDirected(adj)
    error(ERRMSG_DIRECTED);
end;

if ~isConnected(adj)
    error(ERRMSG_NOTCONNECTED);
end;

% -- function body --

% set priority 0 to the first element -> first key for iteration
% as a consequence all remaned elements are set to 'inf'
prior = 0;
% FIXME: prior = [4 1 3 2 16 9 10 14 8 7];
Q = buildQ(ewadj,prior);

% create parent list for MST
sizeew = size(ewadj,1);
% parent list are set to NIL
parent = zeros(sizeew,1);

% do while there is element in the priority queue
while ~isempty(Q)
    [mne,Q] = extractQMin(Q); % extract min priority element in the 1st iteration: 
    mneItem = mne{1,1};                                      % take the item of the min element
    [r,c,v] = find(ewadj(mneItem,1:sizeew));                 % list of neightbours of min element
    for i=1:length(c)
        % is this the first occurance of the node? 
        % if findQ returns 0, the the given value is investigated previously
        if (findQ(Q,c(i)) ~= 0)
            if v(i) < Q{findQ(Q,c(i)),2}                     % priority is less than the actual
                parent(c(i)) = mneItem;                    % set parent
                Q{findQ(Q,c(i)),2} = v(i);                   % set priority
            end;
        end;
    end;
end;

mstadj = zeros(sizeew);
for i=2:sizeew
    mstadj(i,parent(i)) = 1;
end;

% create unidirected graph
mstadj = mstadj + mstadj';
    
% end;