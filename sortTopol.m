function list = sortTopol(daadj)
% topological ordering of DAGs (based on DFS: ordering nodes according to the their finish times in decreasing order) /by Knuth/
%
% Remark: 1) it works on only DAG G(V,E) single connected graph
%         2) DAG expresses partial linear ordering
%         4) result is a directed graph
%
% Complexity: Ordo(V+E) - equals to the depth-first search (DFS)
%
% Syntax:
%   list = sortTopol(daadj)
%   Input params:
%       daadj - DAG graph in adjacency form
%   Return values:
%      list   - topological sort of the vertices
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23.4, Topological sort
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: DFS, isDAG

% Copyright 2006-2007

% -- global constants --
global ERRMSG_NOTDAG;

% -- input checking --
[b, str] = isValidAdj(daadj);
if ~b
    error(str);
end;

% DAG input checking
if ~isDAG(daadj)
    error(ERRMSG_NOTDAG);
end;

% -- function body --

[dff,d,f] = DFS(daadj);
% sorting descendant order of finish values of nodes (nodes are in 1:n), 
% so "list" contains the node list in ascending order according to its finish
% values
[tmp_f,list] = sort(f,'descend');

% end;