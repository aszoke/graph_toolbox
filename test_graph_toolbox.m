%% GRAPH TOOLBOX - SAMPLE TEST SET
% Graph Toolbox contains useful functions to deal with graph.
%
% *Author*: Akos Szoke (aszoke@mit.bme.hu)
%
% *Web site*: <http://www.kese.hu>
%
% *Toolbox Source*: <https://bitbucket.org/aszoke/>
%

%%
dbstop if error;
clear;
globalConsts();

disp(' *********************************************** ');
disp(' ********** GRAPH UTILITY TEST: Start ********** ')
disp(' *********************************************** ');

%% GRAPHANALYSIS
disp(' *** TEST GRAPHANALYSIS: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 471,472
B = [0 1 0 1 0 0;
     0 0 0 0 1 0;
     0 0 0 0 1 1;
     0 1 0 0 0 0;
     0 0 0 1 0 0;
     0 0 0 0 0 1]; % loop on node '6'

% calculate graph metrics
graphanalysis(B);
disp(' *** TEST: Passed *** ')

%% BREADTH FIRST SEARCH
disp(' *** TEST BREADTH FIRST SEARCH: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 471,472
A = [0 1 0 0 1 0 0 0;
     1 0 0 0 0 1 0 0;
     0 0 0 1 0 1 1 0;
     0 0 1 0 0 0 0 1;
     1 0 0 0 0 0 0 0;
     0 1 1 0 0 0 1 0;
     0 0 1 0 0 1 0 1;
     0 0 0 1 0 0 1 0];
graphplot(A,'title','Explorable graph');
bft = revealBFSTree(A,2); % 2-the starting node
graphplot(bft,'title','Breadth-first tree graph');

% find the path between 2 and 8 in "A"
p = findPath(A,2,8);
p2 = findPath(A,8,2);
disp(' *** TEST: Passed *** ')

%% DEPTH FIRST SEARCH
disp(' *** TEST DEPTH FIRST SEARCH: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 478,479
B = [0 1 0 1 0 0;
     0 0 0 0 1 0;
     0 0 0 0 1 1;
     0 1 0 0 0 0;
     0 0 0 1 0 0;
     0 0 0 0 0 1]; % loop on node '6'
graphplot(B,'title','Explorable graph');
dff = revealDFSTree(B);
graphplot(dff,'title','Depth-first tree graph');

% DFS core function demontration:
[dff,d,f,cl,cn,ec] = DFS(B);
graphplot(dff,'title','Depth-first tree graph','nodelabel',addNodeLabel(dff,[d;f]));

disp(' *** TEST: Passed *** ')

%% TOPOLOGICAL SEARCH
disp(' *** TEST TOPOLOGICAL SEARCH (based on DFS): Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 486
% Remark: adj node order affects on the finish time of DFS!!!
C = [0 1 0 0 0 0 1 0 0; % shirt
     0 0 1 0 0 0 0 0 0; % tie
     0 0 0 0 0 0 0 0 0; % jacket
     0 0 0 0 0 0 0 0 0; % watch
     0 0 0 0 0 1 0 0 1; % undershorts
     0 0 0 0 0 0 1 0 1; % pants
     0 0 0 1 0 0 0 0 0; % belt
     0 0 0 0 0 0 0 0 1; % socks
     0 0 0 0 0 0 0 0 0];% shoes
graphplot(C,'title','Explorable graph');
% running topological sort 
l = sortTopol(C);
disp(' *** TEST: Passed *** ')

%% STRONGLY CONNECTED COMPONENTS
disp(' *** TEST STRONGLY CONNECTED COMPONENTS (based on DFS): Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 489

% original graph
D = [0 1 0 0 0 0 0 0;
     0 0 1 0 1 1 0 0;
     0 0 0 1 0 0 1 0;
     0 0 1 0 0 0 0 1;
     1 0 0 0 0 1 0 0;
     0 0 0 0 0 0 1 0;
     0 0 0 0 0 1 0 1;
     0 0 0 0 0 0 0 1];

graphplot(D,'title','Explorable graph');
[cl] = findStronglyCC(D);
graphplot(D,'title','Strongly Connected Components graph','nodelabel',addNodeLabel(D,cl));

% producing SCC graph from a graph
[sccg,cn] = produceStronglyCC(D);
graphplot(sccg,'title','Strongly Connected Components graph','nodelabel',addNodeLabel(sccg,cn));

disp(' *** TEST: Passed *** ')

%% SEMI CONNECTED COMPONENTS
disp(' *** TEST SEMI CONNECTED COMPONENTS: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 489

D = [0 1 0 0 0 0 0 0;
     0 0 0 0 1 1 0 0;
     0 0 0 1 0 0 1 0;
     0 0 1 0 0 0 0 1;
     1 0 0 0 0 1 0 0;
     0 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 1;
     0 0 0 0 0 0 0 1];

graphplot(D,'title','Explorable graph');
cl = findSemiCC(D);
graphplot(D,'title','Connected Components graph','nodelabel',addNodeLabel(D,cl));

disp(' *** TEST: Passed *** ')

%% CONNECTED COMPONENTS
disp(' *** TEST CONNECTED COMPONENTS: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 441

E = [0 0 1 0 0 0 0 0 0 0;
     0 0 1 1 0 0 0 0 0 0;
     1 1 0 0 0 0 0 0 0 0;
     0 1 0 0 0 0 0 0 0 0;
     0 0 0 0 0 1 1 0 0 0;
     0 0 0 0 1 0 0 0 0 0;
     0 0 0 0 1 0 0 0 0 0;
     0 0 0 0 0 0 0 0 1 0;
     0 0 0 0 0 0 0 1 0 0;
     0 0 0 0 0 0 0 0 0 0];
    
graphplot(E,'title','Explorable graph');
sets = findCC(E);
graphplot(E,'title','Connected Components graph','nodelabel',addNodeLabel2(E,sets));

disp(' *** TEST: Passed *** ')

%% PRODUCE PRECEDENCE OF CONNECTED COMPONENTS
disp(' *** TEST PRODUCE PRECEDENCE OF CONNECTED COMPONENTS: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 441

E = [0 0 1 0 0 0 0 0 0 0;
     0 0 1 1 0 0 0 0 0 0;
     1 1 0 0 0 0 0 0 0 0;
     0 1 0 0 0 0 0 0 0 0;
     0 0 0 0 0 1 1 0 0 0;
     0 0 0 0 1 0 0 0 0 0;
     0 0 0 0 1 0 0 0 0 0;
     0 0 0 0 0 0 0 0 1 0;
     0 0 0 0 0 0 0 1 0 0;
     0 0 0 0 0 0 0 0 0 0];

% using no precedence relations between elements: prec3 = zeros(size(E,1));

% using precedences:
prec3 = full(sparse([1 2 5 8 3 2 8],[2 3 6 10 5 6 2],ones(1,7),size(E,1),size(E,1)));
 
graphplot(E,'title','Explorable graph');
graphplot(prec3,'title','Precedence graph');
[sets,cn,cp] = produceCCPrec(E,prec3);
graphplot(E,'title','Connected Components graph','nodelabel',addNodeLabel2(E,sets));
graphplot(cp,'title','Precedence of Connected Components graph','nodelabel',addNodeLabel(cp,cn));

disp(' *** TEST: Passed *** ')

%% MINIMAL SPANNING TREE
disp(' *** TEST MINIMAL SPANNING TREE: Start *** ')
% example based on: 
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000, p. 506

FEW = [0  4  0  0  0  0  0  8  0; % a
       4  0  8  0  0  0  0  11 0; % b
       0  8  0  7  0  4  0  0  2; % c
       0  0  7  0  9  14 0  0  0; % d
       0  0  0  9  0  10 0  0  0; % e
       0  0  4 14 10  0  2  0  0; % f
       0  0  0  0  0  2  0  1  6; % g
       8 11  0  0  0  0  1  0  7; % h
       0  0  2  0  0  0  6  7  0]; %i

% important! FEW must be converter to 'adj' form
graphplot(ewadj2adj(FEW),'title','Explorable graph');
mst = findMSTKruskal(FEW);
% Note: the revealed tree is different from the example in the book (but equally valid!)
graphplot(mst,'title','Connected Components graph');

% important! FEW must be converter to 'adj' form
graphplot(ewadj2adj(FEW),'title','Explorable graph');
mst = findMSTPrim(FEW);
% Note: the revealed tree is different from the example in the book (but equally valid!)
graphplot(mst,'title','Connected Components graph');

disp(' *** TEST: Passed *** ')

%%

disp(' *********************************************** ');
disp(' ********** GRAPH UTILITY TEST: End   ********** ')
disp(' *********************************************** ');
