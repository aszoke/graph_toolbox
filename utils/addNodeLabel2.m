function labels = addNodeLabel2(adj,sets)
%  a utility function create node labels from sets of component nodes
%
% Remark: 1) it works on single connected graphs
%
% Syntax:
%   labels = addNodeLabel(adj,sets)
%   Input params:
%       adj     - graph in adjacency form
%      sets     - sets of component nodes
%   Return values:
%      labels     - component identification number of each node
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --

% constructing labels and reveal component nodes
% remark: component nodes are selected as the smallest elements in the component!
clabel = zeros(1,size(adj,1));
cnode = zeros(1,length(sets));
ndx = 0;
for i = 1:length(sets)
    minElement = min(sets{i});
    cnode(1,i) = minElement;
    for j = 1:length(sets{i})
        ndx = ndx + 1;
        clabel(1,ndx) = minElement;
    end;
end;

% concatenate "clabel" into a string
l = cell(1,size(clabel,2));
for i=1:size(clabel,2)         % column iterator
    prefix = '';            % create empty prefix for string separation
    for j=1:size(clabel,1)     % row iterator
        l{i} = strcat(l{i},prefix,num2str(clabel(j,i)));
        prefix = ',';       % separate the next item in the case of concatenation
    end;
end;

labels = [];
for i=1:size(adj,1)
    labels{i} = strcat(num2str(i), '(', l{i},')');
end;
% end;