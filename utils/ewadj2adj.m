function adj = ewadj2adj(ewadj)
% transform edge weighted adjacency matrix (SCG) to adjacency matrix (SCG)
%
% Remark: 1) it works on single connected graphs
%
% Syntax:
%   adj = ewadj2adj(ewadj)
%   Input params:
%       ewadj   - graph in edge weighted adjacency form
%   Return values:
%      adj      - graph in adjacency form
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- function body --

[r,c] = find(ewadj);
adj = zeros(size(ewadj,1));
for i = 1:size(r)
    adj(r(i),c(i)) = 1;
end;

%end;