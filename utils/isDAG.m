function b = isDAG(adj)
% determines whether a SG contains is a DAG (based on isDirected and hasDCircle)
%
% Remark: 1) it works on DIRECTED or NON-DIRECTED G(V,E) single connected graph
%         2) checking is made up the following steps:
%               - first determines whether the graph is directed (isDirected)
%               - second determines whether the graph has directed circle (hasDCycle) 
%
% Complexity: Ordo(V+E)
%
% Syntax:
%   b = isDAG(adj)
%   Input params:
%       adj    - digraph in adjacency form
%   Return values:
%      b - yes/no -it is/isn't a DAG
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23.3, Depth-first search
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: isDirected, hasDCycle

% Copyright 2006-2007

% -- input checking --
% is single connected? 
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --
b = false;
if isDirected(adj)     % is directed?
    if ~hasDCycle(adj)  % has the directed graph cycle?
        b = true;
    end;
end;
    
% end;