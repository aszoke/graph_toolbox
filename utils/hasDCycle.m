function b = hasDCycle(dadj)
% determines whether a DG contains CYCLE (based on DFS: checking whether it contains backward edge)
%
% Remark: 1) it works on DIRECTED G(V,E) single connected graph
%         2) based on the edge classification of the depth-first algorithm, where 
%           classification of edges: 
%           1 - tree edge
%           2 - back edge
%           3 - forward edge
%           4 - cross edge
%           remark: DAG poses no back edges!
%
% Complexity: Ordo(V+E)
%
% Syntax:
%   b = hasDCycle(dadj)
%   Input params:
%       dadj    - digraph in adjacency form
%   Return values:
%      b - yes/no -it has/hasn't got a cycle
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23.3, Depth-first search
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: DFS, isDirected

% Copyright 2006-2007

% -- global consts --
global ERRMSG_NOTDIRECTED;      

% -- input checking --
[b, str] = isValidAdj(dadj);
if ~b
    error(str);
end;

% is directed?
bDirected = isDirected(dadj);
if ~bDirected
    error(ERRMSG_NOTDIRECTED);
end;
% -- function body --

% is acyclic
% call depth-first search algorithm
[dff,d,f,cp,cs,ec] = DFS(dadj);

% making the decision
if ~isempty(find(ec == 2,1))    % find the first 'back edge' (2)
    b = true;                   % has directed cycle
else
    b = false;
end
    
% end;