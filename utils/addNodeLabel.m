function labels = addNodeLabel(adj,lbs)
%  a utility function create node labels from node ids and labels with concatenation
%
% Remark: 1) it works on single connected graphs
%
% Syntax:
%   labels = addNodeLabel(adj,lbs)
%   Input params:
%       adj     - graph in adjacency form
%       lbs     - node labels (column contains nodes!)
%   Return values:
%      labels     - component identification number of each node
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

if size(adj,2) ~= size(lbs,2)
    error('Different label and adj matrix size!');
end;

% -- function body --

% concatenate "lbs" into a string
l = cell(1,size(lbs,2));
for i=1:size(lbs,2)         % column iterator
    prefix = '';            % create empty prefix for string separation
    for j=1:size(lbs,1)     % row iterator
        l{i} = strcat(l{i},prefix,num2str(lbs(j,i)));
        prefix = ',';       % separate the next item in the case of concatenation
    end;
end;

labels = [];
for i=1:size(adj,1)
    labels{i} = strcat(num2str(i), '(', l{i},')');
end;
% end;