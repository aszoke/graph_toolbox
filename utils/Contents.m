% # Utils of the Graph Toolbox
%
% ### Features
% 
% * **addNodeLabel** - a utility function create node labels from node ids and labels with concatenation 
% * **addNodeLabel2** - a utility function create node labels from sets of component nodes
% * **DFS** - core algorithm of the depth-first search /by Moore/
% * **ewadj2adj** - transform edge weighted adjacency matrix (SCG) to adjacency matrix (SCG)
% * **hasDCycle** - determines whether a DG contains CYCLE (based on DFS: checking whether it contains backward edge)
% * **hasLoop** - determines whether an SG contains loop
% * **isConnected** - determines whether a SCG is a connected graph (based on findCC)
% * **isDAG** - determines whether a SG contains is a DAG (based on isDirected and hasDCircle)
% * **isDirected** - determines whether a SG contains any NON-DIRECTED edge
% * **isNonDirected** - determines whether a SG contains only DIRECTED edge
% * **isValidAdj** - validity checking of adjacency matrix which expresses SCG