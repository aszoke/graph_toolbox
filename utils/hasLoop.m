function b = hasLoop(adj)
% determines whether an SG contains loop
%
% Remark: 1) it works on G(V,E) single connected graph
%
% Complexity: Ordo(V)
%
% Syntax:
%   b = hasLoop(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%      b - yes/no -it is/isn't a directed graph
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --

if isempty(find(diag(adj),1))
    b = false;
else
    b = true;
end
    
% end;