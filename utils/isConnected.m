function b = isConnected(adj)
% determines whether a SCG is a connected graph (based on findCC)
%
% Remark: 1) it works on G(V,E) single connected graph
%
% Complexity: Ordo(V+E)
%
% Syntax:
%   b = isConnected(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%      b - yes/no -it is/isn't a connected graph
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- global consts --
global ERRMSG_DIRECTED;

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

if isDirected(adj)
    error(ERRMSG_DIRECTED);
end;

% -- function body --

[cl,cn] = findCC(adj);
if length(cn) == 1
    b = true;
else
    b = false;
end;

% end;