function b = isDirected(adj)
% determines whether a SG contains any NON-DIRECTED edge
%
% Remark: 1) it works on G(V,E) single connected graph
%
% Complexity: Ordo(V)
%
% Syntax:
%   b = isDirected(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%      b - yes/no -it is/isn't a directed graph
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

if isempty(find(adj,1)) % if adj is zerus, then it 'can be' directed
    b = true;
    return;
end;

% -- function body --

% transpose 
tadj = adj';

% subtracting
diff = adj - tadj;
if isempty(find(diff,1))
    b = false;
else
    b = true;
end
    
% end;