function [dfForest,disc,fin,complabel,compnode,edgeClass] = DFS(adj,norder)
% core algorithm of the depth-first search /by Moore/
%
% Description: A search algorithm of a graph which explores all nodes
%   children to the current node before moving on.
%
% Remark: 1) it works on NON-DIRECTED or DIRECTED G(V,E) single connected graphs
%         2) it works on both joint and disjoint graphs
%         3) there is not start node for traversal
%         4) it builds a depth-forest with root nodes
%         5) the traversal state is expressed with node coloring (white/gray/black)
%         6) this implementation works on adjacency matrix graph
%         representation: Ordo(V*V) space requirements
%         7) produced forest equals to the recursive calls
%         8) discover and finishing times have parenthesis structure 
%            it makes well-formed expression
%            see Parenthesis theorem; Nesting-descendant corollary
%         9) result is a directed graph
%         10) classification of edges: 
%               - tree/forward/back/cross edges (see p.481)
%               - remark: DAG poses no back edges!
%         11) "norder" determines the investigation order of nodes in the main
%            cycle of the algorithm to support stronly connected components
%            determination
%         12) the traversal state is expressed with node coloring
%         (white/gray/black)
%         13) recursive function implementation
%         14) it logs the discovery and finish time of reaching of node 
%         15) this implementation works on adjacency matrix graph
%         representation: Ordo(V*V) space requirements
%
% Complexity: Ordo(V+E)
%
% Syntax:
%   [dfForest,disc,fin,complabel,compnode,edgeClass] = DFS(adj, [norder])
%   Input params:
%       adj        - graph in adjacency form
%       norder     - node ordering information 
%   Return values:
%       dfForest   - depth-first forest adjacency graph
%       disc       - discovery time of node
%       fin        - finish time of node
%       edgeClass  - contains the class of edges from:
%           1 - tree edge
%           2 - back edge
%           3 - forward edge
%           4 - cross edge 
%       complabel   - component identification of a node
%       cnode       - forefathers of the dfForest
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23.3, Depth-first search
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: revealDFSTree, isDAG

% Copyright 2006-2007

% -- input checking --
% if no input is given in "norder"
if nargin == 1 
   norder = [];
end; 

if ~isempty(norder)
    if size(adj,1) ~= size(norder)
        error('Size of paremeters ("norder" and "adj") does not match!')
    end
end;

[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --

n = size(adj,1);    % max vertex count

global g_step;        % declare global variable
g_step = 0;           % global step count
global g_adj;
g_adj = adj;
global g_edgesClass;
g_edgesClass = zeros(n,n);
global g_cnode;         % component identification number
g_cnode = [];

% create "empty tree"
gr(1:n) = struct('color',0,'distance',0,'parent',0); % preallocating for speed
for i=1:n
   gr(i).color = 0;         % 0,1,2 - white, gray, black
   gr(i).discover = NaN;    % step count of reaching the given node (to color gray)
   gr(i).finish = NaN;      % step count of leaving the given node (to color black)  
   gr(i).parent = NaN;      % set parent
   gr(i).component = NaN;   % component identification number
end

% component nodes
cnode = [];
k = 0;

for u=1:n               % vertex count
    if isempty(norder)
        j = u;
    else                % using DSF for determination of strongly connected compsonents
        j = norder(u);  
    end
    if gr(j).color == 0         % white
        k = k + 1;
        g_cnode = k;            % actual component id number
        cnode = [cnode,j];      % collecting component id number
        gr = visit(gr, j);      % calling subfunction (!!!)
    end
end
    
% create depth-first tree
dfForest = zeros(n,n);
for i=1:n
    if ~isnan(gr(i).parent)     % root element has no parent therefore its parents is NaN
        dfForest(gr(i).parent,i) = 1;
    end
end

disc = [gr(:).discover];
fin  = [gr(:).finish];
% lists the component nodes (representative forefathers) in ascending order
compnode = sort(cnode,'ascend');
% in each component, each node is denoted by its representative forefather
% node indeces
complabel = zeros(1,size(gr(:),1));
for i=1:size(gr(:),1)
    complabel(i) = cnode(gr(i).component);       
end;
edgeClass = g_edgesClass;

% removing global variable (garbage collection)
clear global g_step;
clear global g_adj;
clear global g_edgesClass;
clear global g_cnode;

% --------------------- SUBFUNCTION ----------------------------
function gr = visit(gr, u)
% Description: it is a recursive function to going depth

% declare global variables
global g_step;        
global g_adj;
global g_edgesClass;
global g_cnode;

gr(u).color = 1;    % set gray to the parent
gr(u).discover = g_step;
g_step = g_step + 1;        % increment global step count
v = find(g_adj(u,:));  % get its neighbours (children)
for i=1:length(v)
      switch gr(v(i)).color
          case 0          % white
            gr(v(i)).parent = u;        % set parent
            gr = visit(gr, v(i));
            g_edgesClass(u,v(i)) = 1;   % set edge class to tree edge
          case 1          % grey
            g_edgesClass(u,v(i)) = 2;   % set edge class to back edge            
          otherwise
            if (gr(u).discover < gr(v(i)).discover)
                g_edgesClass(u,v(i)) = 3;   % set edge class to forward edge
            else
                g_edgesClass(u,v(i)) = 4;   % set edge class to cross edge
            end
      end
end;
gr(u).color = 2; % set black
gr(u).finish = g_step;
gr(u).component = g_cnode;
g_step = g_step + 1;        % increment global step count

% end;