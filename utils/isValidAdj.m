function [b, str] = isValidAdj(adj)
% validity checking of adjacency matrix which expresses SCG
%
% Remark: 1) it works on single connected graphs
%
% Syntax:
%   [b, str] = isValidAdj(adj)
%   Input params:
%       adj     - DAG graph in adjacency form
%   Return values:
%      b   - validity
%      str - message related to validity
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- global consts --
global ERRMSG_NOTVALID;            
global ERRMSG_NOTQUADRATIC;    

% -- function body --

b = true;
str = '';

if size(adj,1) ~= size(adj,1)
    b = false;
    str = ERRMSG_NOTVALID;
end;

% is multiple connected? is negative value in the 'adj' or is empty?
if ~isempty(find(adj>1,1)) || ~isempty(find(adj<0,1)) || isempty(adj)
    b = false;
    str = ERRMSG_NOTQUADRATIC;
end;

%end;