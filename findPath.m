function path = findPath(adj, s, v)
% find (the shortest) path between two nodes on NON-WEIGHTED SCG (based on revealBFSTree) 
%
% Remark: 1) it works on NON-DIRECTED or DIRECTED G(V,E) simple graph
%         2) It based on the BFS tree: it produces the
%            shortest path (on NON-WEIGHTED graphs) from "s" to "v" - see lemma 23.1
%         3) as a consequence of 2) finding the path means: 
%            finding: if "s" is ancestor of "v" then it is on the path 
%            in the reverse searching route (because the graph is a tree!) 
%
% Complexity: Ordo(V+E)
%
% Syntax:
%   path = findPath(adj, s, v)
%   Input params:
%       adj     - breadth-tree in adjacency form
%       s       - starting node of the exploration
%       v       - searchable node 
%   Return values:
%      path   - contains the node list between "s" and "v"
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- global consts --
global ERRMSG_WRONGINPUTNUM;

% -- input checking --
% if no input is given "adj", "s" - starting node, "v" - ending node
if not(nargin == 3)
   error(ERRMSG_WRONGINPUTNUM);
end; 

[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --

% producing the breadth-first tree
bft = revealBFSTree(adj,s); % "s" - the starting node

% list for path between "s" and "v"
path = v;

% "row" - parent of a node in the adj form graph
% set 'something' for step into the iteration
row = NaN;

% finding parent and put it into the path list
while (~isempty(row)) && (row ~= s)
    row = find(bft(:,v));
    if ~isempty(row)
        path = [path,row];
        v = row;
    end;
end;

% if findig "s" is not the exit criteria, then erase "path"
if row ~= s
    path = [];
else
    % horizontal mirroring to reverse "v" -> "s" to "s" -> "v"
    fliplr(path);
end

% end;
