function [sets,cnode] = findCC(adj)
% find the connected components (CC) of graphs on NON-DIRECTED SCG (based on set unioning)
%
% Remark: 1) it works on NON-DIRECTED G(V,E) single connected graph
%         2) when edges not changing over time, the DFS algorithm is
%            better than this one, but, when edges are dynamically changing
%            it is more efficient.
%
% Complexity: Ordo(V+E)
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
%
% In graph theory, a connected component of an undirected graph is a
% subgraph in which any two vertices are connected to each other by paths, 
% and to which no more vertices or edges can be added while preserving its 
% connectivity.
%
% An alternative way to define connected components involves the
% equivalence classes of an equivalence relation that is defined on the vertices 
% of the graph. In an undirected graph, a vertex v is reachable from a vertex u 
% if there is a path from u to v. In this definition, a single vertex is counted 
% as a path of length zero, and the same vertex may occur more than once within 
% a path. Reachability is an equivalence relation, since transitive,
% reflexive, and symmetric.
% 
% ========================================================================
%
% Syntax:
%   [clabel,cnode] = findCC(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%      sets     - sets of component nodes
%      cnode    - forefather node list (root elements of the components)
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 22.1, Disjoint-set Operations
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: isDirected

% Copyright 2006-2007

% -- global consts --
global ERRMSG_DIRECTED;

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

if isDirected(adj)
    error(ERRMSG_DIRECTED);
end;

% -- function body --

% since 'adj' is non-directed, the upper triangular matrix minus diagonal is enough 
ndadj = triu(adj,1); % upper triangular part of matrix /non-directed adj/

% create singleton sets
sets = cell(1,size(ndadj,1));
for i = 1:size(ndadj,1)
    sets{i} = i;
end;

% generate list of edges
[rows,cols] = find(ndadj);

% going through the edges
for i = 1:length(rows)
    % finding sets containing the actual row and column
    for j = 1:length(sets)
        if ismember(rows(i),sets{j})
            setAndx = j;
        end;
        if ismember(cols(i),sets{j})
            setBndx = j;
        end;
    end;
    % unioning verteces sets which have connected components
    if ~ismember(sets{setAndx},sets{setBndx})   
        u = union(sets{setAndx},sets{setBndx});
        sets(setAndx) = {u};
        sets(setBndx) = [];   % delete cell element
    end;
end;

% reveal component nodes (selected as the smallest elements in the component)
cnode = zeros(1,length(sets));
for i = 1:length(sets)
    cnode(i) = min(sets{i});
end;

% end;