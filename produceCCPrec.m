function [sets,cnode,cprec] = produceCCPrec(copl,prec)
% PRODUCECCPREC produce precedence matrix for the connected components (CC) of graphs on NON-DIRECTED SCG (based on set unioning)
%
% Syntax:
%   [clabel,cnode,cprec] = produceCCPrec(copl,prec)
%   Input params:
%      copl     - coupling matrix between elements
%      prec     - precedence matrix between elements
%   Return values:
%      sets     - sets of component nodes
%      cnode    - forefather node list (root elements of the components)
%      cprec    - precedence matrix between CC's elements
%
% Reference:
% (Szoke:???)
%
% Remark: 1) it works on NON-DIRECTED G(V,E) single connected graph
%
% Complexity: -
% Space     : -
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
%
% In graph theory, a connected component of an undirected graph is a
% subgraph in which any two vertices are connected to each other by paths, 
% and to which no more vertices or edges can be added while preserving its 
% connectivity.
%
% An alternative way to define connected components involves the
% equivalence classes of an equivalence relation that is defined on the vertices 
% of the graph. In an undirected graph, a vertex v is reachable from a vertex u 
% if there is a path from u to v. In this definition, a single vertex is counted 
% as a path of length zero, and the same vertex may occur more than once within 
% a path. Reachability is an equivalence relation, since transitive,
% reflexive, and symmetric.
% 
% ========================================================================
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: isDirected, isDAG, findCC

% Copyright 2006-2009

% -- global consts --
global ERRMSG_DIRECTED;

% -- input checking --
[b, str] = isValidAdj(copl);
if ~b
    error(str);
end;

if isDirected(copl)
    error(ERRMSG_DIRECTED);
end;

% precedence checking
if (size(prec,1) ~= size(copl,1)) || (size(prec,2) ~= size(copl,2))
    error('Size of precedence matrix is not consistent with graph!');
end;

if ~isDAG(prec)
    error('Precedece matrix is not a simple (loop free, single connected) DAG!');
end;

% -- function body --

% since 'copl' is non-directed, the upper triangular matrix minus diagonal is enough 
ndadj = triu(copl,1); % upper triangular part of matrix /non-directed copl/

% create singleton sets
sets = cell(1,size(ndadj,1));
for i = 1:size(ndadj,1)
    sets{i} = i;
end;

% find list of edges
[rows,cols] = find(ndadj);

% going through the edges according to the rows
for i = 1:length(rows)
    ndxA = 0; ndxB = 0;
    j = 1;
    while (j <= length(sets))  ...           % finding sets containing the actual row and column AND
          && (ndxA*ndxB == 0)         % we not found the indexes of the items
        if ismember(rows(i),sets{j})
            ndxA = j;                    % item rows(i) /from node/ is index jth set of the sets
        end;
        if ismember(cols(i),sets{j})
            ndxB = j;                    % item cols(i) /to node/ is index jth set of the sets
        end;
        j = j+1;
    end;
    % unioning verteces sets which have connected components
    if ~ismember(sets{ndxA},sets{ndxB})       % if the two item is not in the same set
        u = union(sets{ndxA},sets{ndxB});
        sets(ndxA) = {u};
        sets(ndxB) = [];   % delete cell element
    end;
end;

% reveal component nodes (selected as the smallest elements in the component)
cnode = zeros(1,length(sets));
for i = 1:length(sets)
    cnode(i) = min(sets{i});
end;

cprec = zeros(length(sets),length(sets));   % initialize component precedence matrix
% fine list of precedence edges
[rows,cols] = find(prec);
% going through the edges according to the rows
for i = 1:length(rows)
    ndxA = 0; ndxB = 0;
    j = 1;
    while (j <= length(sets))  ...           % finding sets containing the actual row and column AND
          && (ndxA*ndxB == 0)         % we not found the indexes of the items
        if ismember(rows(i),sets{j})
            ndxA = j;                    % item rows(i) /from node/ is index jth set of the sets
        end;
        if ismember(cols(i),sets{j})
            ndxB = j;                    % item cols(i) /to node/ is index jth set of the sets
        end;
        j = j+1;
    end;
    % to get a valid precedence graph, it needs the following properties:
    % - single connected: merge can produce multiple connection -> so we
    % can deal with it  (see above *)
    % - loop free: merge can produce loops -> so we can deal with it 
    % - not DAG graphs: merge cannot produce it
    if (ndxA ~= ndxB)                 % it is not a loop
        cprec(ndxA,ndxB)=1;
    end;
end;

% --- EOF ---