function bfTree = revealBFSTree(adj,s)
% breadth-first search (graph traversal algorithm) on JOINT SG
%
% Description: A search algorithm of a graph which explores all nodes
%   adjacent to the current node before moving on.
%
% Remark: 1) it works on NON-DIRECTED or DIRECTED G(V,E) simple graph
%         2) it works on joint graphs
%         3) the traversal starts in the node 's'
%         5) it builds a breadth-tree with root node 's'
%         6) the distance from 's' is continuously increased
%         7) the traversal state is expressed with node coloring (white/gray/black)
%         8) the resulted breadth-tree gives the shortest paths between
%         (i,j) nodes on NOT weighted graphs (special case)
%         9) result is a directed graph
%         10) this implementation works on adjacency matrix graph
%         representation: Ordo(V*V) space requirements
%
% Complexity: Ordo(V+E)
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% In graph theory, breadth-first search (BFS) is a graph search algorithm
% that begins at the root node and explores all the neighboring nodes. Then 
% for each of those nearest nodes, it explores their unexplored neighbor nodes, 
% and so on, until it finds the goal / builds the tree.
%
% BFS is an uninformed search method that aims to expand and examine all
% nodes of a graph or combination of sequences by systematically searching 
% through every solution. In other words, it exhaustively searches the entire 
% graph or sequence without considering the goal until it finds it. It does 
% not use a heuristic algorithm.
%
%  Applications:
% - Finding all connected components in a graph.
% - Finding the shortest path between two nodes u and v 
% - Testing a graph for bipartiteness
% - Cheney's garbage collection algorithm
%
% Exploration sequence:
%            1
%          / | \
%         2  3  4
%        / \   /|\
%       5   6 7 8 9
% ========================================================================
%
% Syntax:
%   bfTree = revealBFSTree(adj, [s])
%   Input params:
%       adj     - graph in adjacency form
%       s       - starting node of the exploration
%   Return values:
%      bfTree   - breadth-first tree adjacency graph
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23.2, Breadth-first search
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --
% if no input is given in "s" - starting node
if nargin == 1 
   s = 1;
end; 

[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --
n = size(adj,1); % max vertex count

% create "empty tree"
gr(1:n) = struct('color',0,'distance',0,'parent',0); % preallocating for speed
for i=1:n
   gr(i).color = 0; % 0,1,2 - white, gray, black
   gr(i).distance = inf;
   gr(i).parent = NaN;
end

% set the root element
gr(s).color = 1; % 0,1,2 - white, gray, black
gr(s).distance = 0;
gr(s).parent = NaN;

% initializing queue
Q = s;
while ~isempty(Q)
   u = Q(1);            % get the first element of the queue
   v = find(adj(u,:));  % get its neighbours' indeces
   for i=1:length(v)
      if gr(v(i)).color == 0        % white
        gr(v(i)).color = 1;         % set children of the actual parent to gray
        gr(v(i)).distance = gr(u).distance + 1; % increment its distance
        gr(v(i)).parent = u;        % set parent
        Q = [Q,v(i)]; % put this element into the Q
      end
   end
   Q = Q(1,2:size(Q,2));    % remove the actual parent element from the Q
   gr(u).color = 2;         % set the actual parent to black
end

% create breadth-first tree
bfTree = zeros(n,n);
for i=1:n
    if ~isnan(gr(i).parent)     % root element has no parent therefore its parent is NaN
        bfTree(gr(i).parent,i) = 1;
    end
end

% end;