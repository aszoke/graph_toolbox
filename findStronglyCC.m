function [clabel,cnode] = findStronglyCC(adj)
% find the strongly connected components (SCC) of graphs on SCG (based on DFS: applied DFS on G and G')
%
% Remark: 1) it works on NON-DIRECTED or DIRECTED G(V,E) single connected graph
%         2) a component is strongly connected iff u->v AND v->u in
%         G'(V',E'):u,v elements of V', and G'<=G
%
% Complexity: Ordo(V+E) - as DFS
%
% Syntax:
%   [clabel,cnode] = findStronglyCC(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%      clabel   - component identification number of each node
%      cnode    - forefather node list (root elements of the components)
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23.5, Strongly Connected Components
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: DFS,isValidAdj

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --
% determining finish time of each node with DFS algorithm
[dff,d,f] = DFS(adj);

% in orer to determine the nodes from which the forefather is reachable, we
% produce the transpose of the origin "adj" graph
tadj = adj';

% sorting descending order of finish values of nodes (nodes are in 1:n), 
% so "list" contains the node list in ascending order according to its finish
% values
[tmp_f,list] = sort(f,'descend');

% calling DFS with finish date ordering information of nodes to get the
% strongly connected components of "tadj" in "dff"
[dff,d,f,cl,cn] = DFS(tadj, list);

clabel = cl;
cnode = sort(cn,'ascend');

% end;