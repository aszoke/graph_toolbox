function graphanalysis(adj)
% some important metrics calculations on SG
%
% Complexity: -
%
% Syntax:
%   m = graphmetrics(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%       m       - metrics
%
% Reference: 
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 23 Elementary Graph Algorithms
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --

% ===== NOTIONS =====
% graph                  : G = (V,E)
% edge series            : a (n0,e1,n1,e2,n2,...,nk) ordered series
% walk between n0 and nk : an edge series, where nodes can be repeated, and
%                          edges can NOT be repeated
%   (it can be represented as (n0,e1,n1,e2,n2,...,nk) - ordered sequence
% path between n0 and nk : a walk, where nodes can NOT be repeated
%   (it can be represented as (n0,n1,...,nk) - ordered sequence
% circle                 : a path, where n0=nk (closed path)
% Euler walk             : a walk, where n0=nk (closed walk) and 
%                          all edges are traversed
% Hamilton circle        : a circle, where all edges are traversed

%% Graph measures
% order            : |V|
% size             : |E|
% minDegree        : min number of incident nodes of a node in a graph
% maxDegree        : max number of incident nodes of a node in a graph
% avgDegree        : average number of incident nodes of a node in a graph
% edgeNodeRation   : edge-node ratio in a graph
% diameter         : (for connected graph or strongly connected digraph) the
%                   greatest distance (between nodes) in a graph

orderA = size(adj,1);
sizeA  = size(find(adj),1);
minDegreeA = inf;
maxDegreeA = 0;

sumDegreeA = 0;
for i=1:size(adj,1)
    rowA = adj(i,:);
    X = find(rowA);
    sumDegreeA = sumDegreeA + size(X,2);
    if size(X,2) < minDegreeA
        minDegreeA = size(X,2);
    end;
    if size(X,2) > maxDegreeA
        maxDegreeA = size(X,2);
    end;
end;
avgDegreeA = sumDegreeA / size(adj,1);

edgeNodeRationA = 0.5 * avgDegreeA;

diameterA = 0;
for i=1:size(adj,1)
    for j=i+1:size(adj,1)
        len = length(findPath(adj,i,j)) - 1;
        if len > diameterA
            diameterA = len;
        end;
    end;
end;

%% Node measures
% in degree  : the number of head endpoints adjacent to a node
% out degree : the number of tail endpoints adjacent to a node

for i=1:size(adj,1)
    rowA = adj(i,:);
    colA = adj(:,i);
    outDegree(1,i) = length(find(rowA));
    inDegree(1,i)  = length(find(colA));    
end;

%% General decidable questions
% isDirected: the edges have a direction associated with them
% hasLoop: has an edge that connects a vertex to itself

bDirected = isDirected(adj);
bLoop = hasLoop(adj);

%% Special  decidable questions on digraph
% hasDCycle: has a number of vertices connected in a closed chain

bDCycle = hasDCycle(adj);

%%

disp('*** Graph analysis ***');
disp('=== Graph Properties  ===');
disp(['Directed?      :        ',mat2str(bDirected)]);
disp(['Has loop?      :        ',mat2str(bLoop)]);
disp(['Has d-circle?  :        ',mat2str(bDCycle)]);
disp('=== Graph Measurement ===');
disp('-- Direct measurement :');
disp(['Size  (|E|)    :        ',num2str(sizeA)]);
disp(['Order (|V|)    :        ',num2str(orderA)]);
disp(['Minimum degree :        ',num2str(minDegreeA)]);
disp(['Maximum degree :        ',num2str(maxDegreeA)]);
disp('-- Derived measurement:');
disp(['Average degree :        ',num2str(avgDegreeA)]);
disp(['Edge-node ratio:        ',num2str(edgeNodeRationA)]);
disp(['Diameter       :        ',num2str(diameterA)]);
disp('=== Node Measurement ===');
disp(['In degree      :        ',num2str(inDegree)]);
disp(['Out degree     :        ',num2str(outDegree)]);

