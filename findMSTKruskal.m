function mstadj = findMSTKruskal(ewadj)
% find the minimum spanning tree (MFT) of graphs on NON-DIRECTED SCG /by Kruskal/
%
% Remark: 1) it works on NON-DIRECTED G(V,E) single connected graph
%         2) it is a greedy algorithm
%
% Complexity: Ordo(ElgE)
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% Given a connected, undirected graph, a spanning tree of that graph is a
% subgraph which is a tree and connects all the vertices together. A single 
% graph can have many different spanning trees. We can also assign a weight 
% to each edge, which is a number representing how unfavorable it is, and 
% use this to assign a weight to a spanning tree by computing the sum of the 
% weights of the edges in that spanning tree. A minimum spanning tree (MST) 
% or minimum weight spanning tree is then a spanning tree with weight less 
% than or equal to the weight of every other spanning tree.
% ========================================================================
%
% Syntax:
%   mstadj = findMSTKruskal(ewadj)
%   Input params:
%       ewadj   - edge weight adjacency form
%   Return values:
%      mstadj   - minimum spanning tree in adjacency form
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 24.2, Kruskal's algorithm
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: isDirected,findMSTPrim

% Copyright 2006-2007

% -- global consts --
global ERRMSG_DIRECTED;
global ERRMSG_NOTCONNECTED;

% create adjacency matrix from weighted adjacency matrix 
% (for input checking)
adj = ewadj2adj(ewadj);

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

if isDirected(adj)
    error(ERRMSG_DIRECTED);
end;

if ~isConnected(adj)
    error(ERRMSG_NOTCONNECTED);
end;

% -- function body --

% since 'ewadj' is non-directed, the upper triangular matrix minus diagonal is enough 
ndewadj = triu(ewadj,1); % upper triangular part of matrix /non-directed adj/

% create singleton sets
sets = cell(1,size(ndewadj,1));
for i = 1:size(ndewadj,1)
    sets{i} = i;
end;

% sorting edges ([roweds,colews]) according to the weights' values (vews)
[rowews,colews,vews] = find(ndewadj);
ew = [rowews,colews,vews]; 
sew = sortrows(ew,3);

% minimum spanning tree
mstadj = zeros(size(ndewadj));
% going through the edges
for i = 1:size(sew,1)
    % finding sets containing the actual row and column
    for j = 1:length(sets)
        if ismember(sew(i,1),sets{j}) % which set contains the actual 'from' node
            setAndx = j;
        end;
        if ismember(sew(i,2),sets{j}) % which set contains the actual 'to' node
            setBndx = j;
        end;
    end;
    % unioning verteces sets (containing the previous 'from' and 'to' nodes) which have connected components
    if ~ismember(sets{setAndx},sets{setBndx})   
        u = union(sets{setAndx},sets{setBndx});
        sets(setAndx) = {u};
        sets(setBndx) = [];   % delete cell element
        mstadj(sew(i,1),sew(i,2)) = 1;
    end;
end;

% create non-directed graph
mstadj = mstadj + mstadj';

% end;