function [sccadj,cp] = produceStronglyCC(adj)
% produce strongly connected components (SCC) graph from an SCG (based on findSCC)
%
% Remark: 1) it works on NON-DIRECTED or DIRECTED G(V,E) single connected graph
%
% Complexity: Ordo(V+E) - as DFS
%
% Syntax:
%   sccadj = produceStronglyCC(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%      sccadj   - adj matrix of strongly connected component graph
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: findSCC

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --
% calling findSCC to determine (directed) scc nodes of a graph
% cl - node labels, cp - components in ascending order
[cl,cp] = findStronglyCC(adj);

% create an empty same size adj for scc graph
sccadj = zeros(size(adj));

% truncate SCC adj matrix, delete nodes which are not forefaher of the
% components (which only have connections with nodes in the same component)
for i=1:size(adj,1)
   v = find(adj(i,:));          % get its neighbours' indeces
   for j=1:length(v)
      if cl(i) ~= cl(v(j))      % create edge between nodes which are in different components
          if sccadj(cl(i),cl(v(j))) ~= 1
              sccadj(cl(i),cl(v(j))) = 1; % create edge between node
          end;
      end
   end
end;

% compress scc matrix: erease those nodes which have no node neighbour
j = 0;  % iterator for col/row deletion
for i=1:size(adj,1)
    j = j + 1;
   if isempty(find(sccadj(j,:),1)) && isempty(find(sccadj(:,j),1))
       sccadj(j,:) = [];    % delete row
       sccadj(:,j) = [];    % delete column
       % index decrement because of the deletion!
       j = j - 1;
   end; 
end;

% end;

