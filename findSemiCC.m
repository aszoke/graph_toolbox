function [clabel,cnode] = findSemiCC(adj)
% find the semi connected components (SemiCC) of graphs on SCG (based on DFS: complabels)
%
% Remark: 1) it works on NON-DIRECTED or DIRECTED G(V,E) single connected graph
%         2) a component is semi connected iff u->v OR v->u in
%         G'(V',E'):u,v elements of V', and G'<=G
%
% Complexity: Ordo(V+E) - as DFS
%
% Syntax:
%   [clabel,cnode] = findSemiCC(adj)
%   Input params:
%       adj     - graph in adjacency form
%   Return values:
%      clabel   - component identification number of each node
%      cnode    - forefather node list (root elements of the components)
%
% Reference: -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2007

% -- input checking --
[b, str] = isValidAdj(adj);
if ~b
    error(str);
end;

% -- function body --
% determining finish time of each node with DFS algorithm
[dff,d,f,clabel,cnode] = DFS(adj);

% end;